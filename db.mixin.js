"use strict";

const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = function (model) {
  const cacheCleanEventName = `cache.clean.${model.name}`;
  const schema = {
    name: model.name,
    collection: model.name,
    mixins: [DbService],
    adapter: new MongoDBAdapter(
      `mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASS}@cluster0.hkwwn6i.mongodb.net/?retryWrites=true&w=majority`
    ),
    events: {
      /**
       * Subscribe to the cache clean event. If it's triggered
       * clean the cache entries for this service.
       *
       * @param {Context} ctx
       */
      async [cacheCleanEventName]() {
        if (this.broker.cacher) {
          await this.broker.cacher.clean(`${this.fullName}.*`);
        }
      },
    },
    actions: {
      // create: false,
      // update: false,
      // remove: false,
    },

    methods: {
      /**
       * Send a cache clearing event when an entity changed.
       *
       * @param {String} type
       * @param {any} json
       * @param {Context} ctx
       */
      async entityChanged(type, json, ctx) {
        ctx.broadcast(cacheCleanEventName);
      },
    },

    async started() {
      // Check the count of items in the DB. If it's empty,
      // call the `seedDB` method of the service.
      if (this.seedDB) {
        const count = await this.adapter.count();
        if (count == 0) {
          this.logger.info(
            `The '${model.name}' collection is empty. Seeding the collection...`
          );
          await this.seedDB();
          this.logger.info(
            "Seeding is done. Number of records:",
            await this.adapter.count()
          );
        }
      }
    },
  };

  return schema;
};
