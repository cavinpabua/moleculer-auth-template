const { MoleculerError } = require("moleculer").Errors;

const getToken = (ctx) => {
  const token =
    ctx.options.parentCtx.params.req.headers.authorization &&
    ctx.options.parentCtx.params.req.headers.authorization.replace(
      "Bearer ",
      ""
    );
  return token;
};

const success = (message, data = {}) => {
  return {
    success: true,
    message,
    data,
  };
};

const error = (message) => {
  throw new MoleculerError(message);
};

const fail = (error, data = {}) => {
  return {
    success: false,
    error,
    data,
  };
};

const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

module.exports = {
  getToken,
  success,
  fail,
  validateEmail,
};
