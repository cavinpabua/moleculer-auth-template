"use strict";
const argon2 = require("argon2");
const DbMixin = require("../mixins/db.mixin");
const Sequelize = require("sequelize");
const jwt = require("jsonwebtoken");
const utils = require("./utils");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
  name: "users",

  /**
   * Settings
   */
  settings: {
    routes: [
      {
        authentication: false,
        authorization: false,
      },
    ],
    JWT_SECRET: process.env.JWT_SECRET || "jwt-conduit-secret",
    /** Public fields */
    fields: [
      "_id",
      "email",
      "image",
      "firstName",
      "middleName",
      "lastName",
      "role",
      "verified",
      "isActive",
    ],
    /** Validator schema for entity */
    entityValidator: {
      password: { type: "string", min: 8 },
      email: { type: "email" },
      image: { type: "string", optional: true },
    },
  },
  mixins: [DbMixin()],
  /**
   * Dependencies
   */
  dependencies: [],
  /**
   * Actions
   */
  actions: {
    /**
     * Register User
     *
     * @param {String} email - User email
     * @param {String} password - User password
     * @param {String} firstName - User First Name
     * @param {String} middleName - User Middle Name
     * @param {String} lastName - User Last Name
     * @returns {Object}
     */
    register: {
      rest: {
        method: "POST",
        path: "/register",
      },
      params: {
        email: { type: "email" },
        password: "string",
        mobileNumber: "string",
      },
      async handler(ctx) {
        const params = ctx.params;
        const existing = await this.adapter.findOne({
          email: params.email,
        });

        try {
          if (!existing) {
            // Create User
            if (!utils.validateEmail(params.email))
              return utils.fail("Invalid email format");
            const hash = await argon2.hash(ctx.params.password);
            await this.adapter.insert({
              ...params,
              password: hash,
            });
            delete params.password;
            const data = {
              user: {
                ...params,
              },
            };
            return utils.success("Registration Successful!", data);
          } else {
            return utils.fail("User name already exists", {});
          }
        } catch (e) {
          return utils.fail(JSON.stringify(e), {});
        }
      },
    },

    /**
     * Login User
     *
     * @param {String} email - User email
     * @param {String} password - User password
     * @returns {String} - Token of user
     */
    login: {
      rest: {
        method: "POST",
        path: "/login",
      },
      params: {
        email: { type: "email" },
        password: "string",
      },
      /** @param {Context} ctx  */
      async handler(ctx) {
        const params = ctx.params;
        const existing = await this.adapter.findOne({
          email: params.email,
        });
        if (existing) {
          if (await argon2.verify(existing.password, params.password)) {
            const token = jwt.sign(
              { email: params.email, id: existing._id },
              this.settings.JWT_SECRET,
              {
                expiresIn: "7d",
              }
            );
            const data = {
              token,
            };
            return utils.success("Login successful!", data);
          }
        }
        return utils.fail("Invalid email and password!");
      },
    },

    /**
     * Logout User
     *
     * @returns true of false
     */
    logout: {
      auth: "required",
      rest: {
        method: "POST",
        path: "/logout",
      },
      /** @param {Context} ctx  */
      async handler(ctx) {
        const token = utils.getToken(ctx);
        if (token) {
          const tokenKey = `bl_${token}`;
          await this.broker.cacher.set(tokenKey, token);
          return utils.success("Logout successful!");
        }
        return utils.fail("Token is required!");
      },
    },

    /**
     * Update User Password
     *
     * @param {String} newPassword - New Password of User
     * @returns {Boolean} success
     */
    updatePassword: {
      rest: {
        method: "POST",
        path: "/update-password",
      },
      params: {
        newPassword: { type: "string", min: 8 },
      },
      /** @param {Context} ctx  */
      async handler(ctx) {
        const token = await utils.getToken(ctx);
        if (token) {
          const user = jwt.verify(token, this.settings.JWT_SECRET);
          const hash = await argon2.hash(ctx.params.newPassword);
          await this.adapter.updateById(user.id, {
            $set: {
              password: hash,
            },
          });
          return utils.success("Password changed successfully!");
        }

        return utils.fail("Token is required!");
      },
    },
  },

  /**
   * Events
   */
  events: {},

  /**
   * Methods
   */
  methods: {},

  /**
   * Service created lifecycle event handler
   */
  created() {},

  /**
   * Service started lifecycle event handler
   */
  async started() {},

  /**
   * Service stopped lifecycle event handler
   */
  async stopped() {},
};
